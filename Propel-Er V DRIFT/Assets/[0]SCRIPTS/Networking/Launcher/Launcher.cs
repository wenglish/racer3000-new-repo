﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Com.YahManGames.P3K
{
    public class Launcher : Photon.PunBehaviour
    {

        public static Launcher ins;


        #region Public Variables
        /// <summary>
        /// The PUN loglevel. 
        /// </summary>
        public PhotonLogLevel Loglevel = PhotonLogLevel.Informational;


        /// <summary>
        /// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
        /// </summary>   
        [Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
        public byte MaxPlayersPerRoom = 2;



        #endregion


        #region Private Variables


        /// <summary>
        /// This client's version number. Users are separated from each other by gameversion (which allows you to make breaking changes).
        /// </summary>
        string _gameVersion = "1";


        /// <summary>
        /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon, 
        /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
        /// Typically this is used for the OnConnectedToMaster() callback.
        /// </summary>
        bool isConnecting;

        #endregion


        #region MonoBehaviour CallBacks


        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
        /// </summary>
        void Awake()
        {

            ins = this;


            // #Critical
            // we don't join the lobby. There is no need to join a lobby to get the list of rooms.
            PhotonNetwork.autoJoinLobby = false;


            // #Critical
            // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
            PhotonNetwork.automaticallySyncScene = true;



            // #NotImportant
            // Force LogLevel
            PhotonNetwork.logLevel = Loglevel;
        }


        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during initialization phase.
        /// </summary>
        void Start()
        {

            //Connect();

        }


        #endregion


        #region Public Methods


        /// <summary>
        /// Start the connection process. 
        /// - If already connected, we attempt joining a random room
        /// - if not yet connected, Connect this application instance to Photon Cloud Network
        /// </summary>
        public void Connect()
        {
            print("CONNECT via Launcher ");

            // keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
            isConnecting = true;



            // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
            if (PhotonNetwork.connected)
            {
                // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnPhotonRandomJoinFailed() and we'll create one.
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                // #Critical, we must first and foremost connect to Photon Online Server.
                PhotonNetwork.ConnectUsingSettings(_gameVersion);
            }
        }


        #endregion









        #region Photon.PunBehaviour CallBacks


        public override void OnConnectedToMaster()
        {
            // we don't want to do anything if we are not attempting to join a room. 
            // this case where isConnecting is false is typically when you lost or quit the game, when this level is loaded, OnConnectedToMaster will be called, in that case
            // we don't want to do anything.
            if (isConnecting)
            {
                // #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnPhotonRandomJoinFailed()
                PhotonNetwork.JoinRandomRoom();
            }
        }


        public override void OnDisconnectedFromPhoton()
        {
            Debug.LogWarning("DemoAnimator/Launcher: OnDisconnectedFromPhoton() was called by PUN");
        }



        public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
        {
            Debug.Log("DemoAnimator/Launcher:OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 2}, null);");
            // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
            PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = MaxPlayersPerRoom }, null);

        }




        //this value is assigned when entered room, if 0 or 1 player already exists. 
        //it will define if player is on left or right of screen to start. 
        public override void OnJoinedRoom()
        {

            Debug.Log("Player has joined room, with curent player count = " + PhotonNetwork.room.PlayerCount);


            int playerCount = PhotonNetwork.room.PlayerCount;
            // #Critical: We only load if we are the first player, else we rely on  PhotonNetwork.automaticallySyncScene to sync our instance scene.
            if (playerCount == 1)
            {
                //cache player as int
                //Loading_Mng.playerAsInt = 1;
                List<string> s = new List<string> { "You Are Connected!", "Please Wait..." };
                Home_Mng.ins.init_animateAlert(s, 1);
                print("!!!!!!! MUST REINSTALL TIME WINDOW.");
                StartCoroutine(initWatchTimeLimit());
                LoadingMng.localPlayerInt = 1;
            }
            else if (playerCount == 2)
            {
                //cache player as int
                //Loading_Mng.playerAsInt = 2;
                List<string> s = new List<string> { "Player 2 Connected!", "Please Wait..." };
                Home_Mng.ins.init_animateAlert(s, 1);
                //GameOptions_Mng.ins.onlineOpponentFound();
                LoadingMng.localPlayerInt = 2;
            }

        }



        /// <summary>
        /// purpose of this iniWatchTimeLimit() is to set up solo play if polayer waits too long, 
        /// for oppopient, even if online. 
        /// </summary>
        /// <returns>The watch time limit.</returns>
        IEnumerator initWatchTimeLimit()
        {



            float initTime = (float)PhotonNetwork.time;
            float timeLimit = 100000000000000000f;
            bool isWatching = true;
            while (isWatching)
            {



                int playerCount = PhotonNetwork.room.PlayerCount;
                if (playerCount == 1)
                {

                    float crntTime = (float)PhotonNetwork.time;
                    float deltaTime = (float)PhotonNetwork.time - initTime;
                    //print("detla time = " + deltaTime);
                    if (deltaTime >= timeLimit)
                    {

                        print("TIME LIMIT PASSED, LOAD SOLO PLAY " + deltaTime);
                        isWatching = false;
                        StopCoroutine(initWatchTimeLimit());
                        GameOptions_Mng.ins.noOnlineOpponentFound();
                    }


                }
                if (playerCount == 2)
                {

                    //
                    isWatching = false;
                    StopCoroutine(initWatchTimeLimit());
                    GameOptions_Mng.ins.onlineOpponentFound();

                }







                yield return null;

            }


        }

        #endregion


    }


}