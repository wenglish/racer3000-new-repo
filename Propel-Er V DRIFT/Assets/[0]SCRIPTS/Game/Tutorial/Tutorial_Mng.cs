﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;









public class Tutorial_Mng : MonoBehaviour {


    public static Tutorial_Mng ins;
    public Tutorial_Unit[] units;
    int requiredInputType;
    [Space]
    public Transform tf_main;
    public Transform tf_fingerMv;
    public Transform tf_fingerRtn;
    public Transform[] tf_rings;
    int ringCount;
    int lastAnimTypeClass;


    public TextMeshPro tmp_info;
    int textCount;
    Vector3 fingerUpRtn = new Vector3(-72.177f,0,9f);
    Vector3 fingerDownRtn = new Vector3(-72.177f, 0, 0);
    Vector3 fingerRightPos = new Vector3(1, 0, 0);
    PlayerUnit playerUnit;
    Rigidbody rgby;



    int tutorialCount;


    public Camera camera;




	private void Awake()
	{
        ins = this;

        tf_main.localScale = Vector3.zero;
        tmp_info.text = "";
        for (int i = 0; i < tf_rings.Length; i++)
        {
            tf_rings[i].localScale = Vector3.zero;
        }
        tf_fingerRtn.localEulerAngles = fingerDownRtn;


        camera.enabled = false;
	}





    private void Start()
    {
     
    }









    //initiated by Game Mng, at local_PlayerCreated() is LoadingMng.isTutorial. 
    public void run_driveTutorial(PlayerUnit player){


        print("run Drive Tutorial. ");

        playerUnit = player;
        rgby = player.playerCntrlr.rgby;


        tf_main.localScale = Vector3.one;




        camera.enabled = true;
        tutorialCount = 0;
        //setDetail();

        Game_Mng.isActive = true;

    }



    void setDetail(){

        print(":::: set detail :: " + tutorialCount);
        //set text. 
        string s = units[tutorialCount].s;
        tmp_info.text = s;



        //set input type. 
        //watched in Update()
        requiredInputType = units[tutorialCount].inputType;

        endFingerAnim();
        if (requiredInputType == 0) {
            //tap.
         
        }
        if (requiredInputType == 1)
        {
            //press
            animateFinger(Vector3.zero, Vector3.zero, Vector3.one, 0.25f, 1);
        }
        if (requiredInputType == 2)
        {
            //drag left
            animateFinger(fingerRightPos * -1, Vector3.zero, Vector3.one, 1, -1);
        }
        if (requiredInputType == 3)
        {
            //drag right
            animateFinger(fingerRightPos, Vector3.zero, Vector3.one, 1, -1);
        }
        if (requiredInputType == 4)
        {
            //release after hold. 


        }


        Time.timeScale = units[tutorialCount].timeScale;
    }



    float time_mouseDown;
    float time_mouseUp;
    float mousexPos_crnt;
    float mousexPos_init;
    bool isTempInputRequiredCompleted;

    void Update(){

        if(Input.GetMouseButtonDown(0)){

            time_mouseDown = Time.time;
            mousexPos_crnt = Input.mousePosition.x;
            mousexPos_init = Input.mousePosition.x;
        }
        if(Input.GetMouseButtonUp(0)){

            time_mouseUp = Time.time;
            float delta = time_mouseUp - time_mouseDown;
            if(delta < 0.125f){
                //tap...

                if(requiredInputType == 0){
                    requiredInputCompleted();
                    return;
                }else{
                    
                }
            }else{
                if (delta > 1.25f)
                {
                    if (requiredInputType == 4 || isTempInputRequiredCompleted == true)
                    {

                        requiredInputCompleted();
                        return;
                    }
                    else
                    {

                    }
                }
            }



        }


        if(Input.GetMouseButton(0)){

            mousexPos_crnt = Input.mousePosition.x;

            float moveDelta = mousexPos_init - mousexPos_crnt;
            //print("move delta " + moveDelta);
            if(moveDelta < -50 || moveDelta > 50){
                print("MOVED LEFT || MOVED RIGHT");
                tempRequiredInputCompleted();
                return;

            } 
    
        }


    }


    void requiredInputCompleted(){
        tutorialCount++;
        if(tutorialCount < units.Length){
            //setDetail();
        }else{
            print("TUTT OVER ");
        }

        isTempInputRequiredCompleted = false;

    }


    void tempRequiredInputCompleted(){


        if(!isTempInputRequiredCompleted){
            print("TEMP INPUT REQUIRED MADE");
            isTempInputRequiredCompleted = true;
           
            string s = "VERY GOOD";
            tmp_info.text = s;

            endAnimateRings();
            endFingerAnim();

        }


    }




   





    void initAnimateFinger(int animTypeClass){
        if(animTypeClass == 0){

            //finger at 0 pos, 0 scale. 

        }
        if(animTypeClass == 1){

            //finger at 0 pos, 1 scale, up rtn
            endAnimateRings();
            tf_fingerMv.localScale = Vector3.one;
            //tf_fingerRtn.localEulerAngles = fingerUpRtn;
        }

        if(animTypeClass == 2){

            //finger at 0 pos, 1 scale, down rtn
            runAnimateRings(0.5f);
            //tf_fingerRtn.localEulerAngles = Vector3.zero;
            tf_fingerMv.localScale = Vector3.one;

        }

        if(animTypeClass == 3){

            //finger at left pos, 1 scale, down rtn
            if(lastAnimTypeClass == 4){

                //moving from right to keft, double time. 
                animateFinger(fingerRightPos * -1, Vector3.zero, Vector3.one, 1, 1);
                endAnimateRings();
                runAnimateRings(1);


            }else{

                //moving from center to edge. normal time 
                animateFinger(fingerRightPos * -1, Vector3.zero, Vector3.one, 0.5f, 1);
                endAnimateRings();
                runAnimateRings(1);



            }

        }

        if(animTypeClass == 4){

            ///finger at right pos, 1 scale, down rtn.
            if(lastAnimTypeClass == 3){

                //moving from left to right, double time. 
                animateFinger(fingerRightPos, Vector3.zero, Vector3.one, 1, 1);
                endAnimateRings();
                runAnimateRings(1);


            }
            else
            {

                //moving from center to edge. normal time 
                animateFinger(fingerRightPos, Vector3.zero, Vector3.one, 0.5f, 1);
                endAnimateRings();
                runAnimateRings(1);



            }


        }



        lastAnimTypeClass = animTypeClass;


        
    }









    void animateFinger(Vector3 pos, Vector3 rtn, Vector3 scale, float time, int loops){



        tf_fingerMv.localScale = scale;
        //tf_fingerRtn.localEulerAngles = rtn;
        tf_fingerMv.DOLocalMove(pos, time, false).SetLoops(loops);


    }
    void endFingerAnim(){

        tf_fingerMv.DOKill();
        //tf_fingerRtn.localEulerAngles = Vector3.zero;
        tf_fingerMv.localScale = Vector3.zero;

    }

    void runAnimateRings(float time){


        for (int i = 0; i < tf_rings.Length; i++){
            
            tf_rings[i].DOScale(Vector3.one, time).SetLoops(-1);

        }
    }




    void endAnimateRings(){

        for (int i = 0; i < tf_rings.Length; i++)
        {

            tf_rings[i].DOKill();
            tf_rings[i].localScale = Vector3.zero;


        }


    }








}
