﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;





namespace Com.YahManGames.P3K
{

    public class GameOptions_Mng : MonoBehaviour
    {



        public static GameOptions_Mng ins;

        public Transform tf_main; //holds solo and multi option. 
        public Transform tf_Offline;//simple package of sprites which show that player is off line so multi is not allowed. 

        bool isOnline;

        float transitionTime; 


        Vector3 posHide_up = new Vector3(0, 45, 0);
        Vector3 posHide_down = new Vector3(0, -15, 0);





        private void Awake()
        {
            ins = this;

        }
        void Start(){
            transitionTime = LoadingMng.transitionTime;
            tf_main.position = posHide_down;
        }




        public void bringIn()
        {


            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                isOnline = false;
                tf_Offline.localScale = Vector3.one;
            }
            else
            {
                isOnline = true;
                tf_Offline.localScale = Vector3.zero;
            }


            runTrans(tf_main, Vector3.zero, Vector3.one, 0.25f, Ease.OutBounce);

        }






        //with no text
        void runTrans(Transform tf, Vector3 pos, Vector3 scl, float delay, Ease ease){
            tf.DOMove(pos, transitionTime, false).SetEase(ease).SetDelay(delay);
            tf.DOScale(scl, transitionTime).SetEase(ease).SetDelay(delay);
        }





        public void bp_solo(){
            bringOut();
            LoadingMng.gameModeInt = 1;
            LoadingMng.localPlayerInt = 1;
            SoloGameOptions_Mng.ins.bringIn();
        }




        public void bp_multi(){
            if(!isOnline){
                ///cant play. aniumate. 
            }
            else{


                //bring out main btns. 
                bringOut();
                LoadingMng.gameModeInt = 2;

                //shift main logo.
                Home_Mng.ins.runTrans(Home_Mng.ins.tf_mainLogo, Home_Mng.ins.posStart_mainLogo, Home_Mng.ins.sclStart_mainLogo, 0.25f, Ease.InOutCirc);


                //bring in the alert mng, of the home mng, with animation. 
                List<string> s = new List<string> { "Connecting...", "Connecting..", "Connecting." };
                Home_Mng.ins.init_animateAlert(s, 1);
                runTrans(Home_Mng.ins.tf_alert, Vector3.zero, Vector3.one, 0.25f, Ease.OutBounce);


                //run connection check. will default to close if no oppoent found. 
                Launcher.ins.Connect();
            
            
            }

        }




        public void onlineOpponentFound()
        {
            //this will take care of setting game mode and load the game over the network.  
            Home_Mng.ins.onlineOpponentFound();

        }
        public void noOnlineOpponentFound()
        {
            //return from Launcher.ins, no online opponent found in time limit. 

            //update alert. 
            List<string> s = new List<string> { "Can't Find Opponent Now", "Please Try Again Later."};
            Home_Mng.ins.init_animateAlert(s, 1);

            Invoke("CloseOnlineCheck", 3);

        }
        void CloseOnlineCheck(){

            //from noOnlineOpponentFound(), at this point, we need to 

            //repostion main logo. 
            Home_Mng.ins.runTrans(Home_Mng.ins.tf_mainLogo, Home_Mng.ins.posEnd_mainLogo, Home_Mng.ins.sclEnd_mainLogo, 0.25f, Ease.InOutCirc);

            //hide and end alert anim
            Home_Mng.ins.cancelAlertAnim();
            Home_Mng.ins.runTrans(Home_Mng.ins.tf_alert, Home_Mng.ins.posHide_up, Vector3.one, 0.25f, Ease.InBounce);

            //bring in these.
            bringIn();
           




        }





        public void bp_back(){
            //bring out this main btns
            bringOut();
            //bring in home main btns
            Home_Mng.ins.runTrans(Home_Mng.ins.tf_mainBtns, Vector3.zero, Vector3.one, 0.25f, Ease.OutBounce);
        }


        public void bp_toggleRecordPlay(){

            if(LoadingMng.isRecordPlay){
                LoadingMng.isRecordPlay = false;
                return;
            }else{
                LoadingMng.isRecordPlay = true;
                return;
            }

        }




    
        public void bringOut()
        {
            runTrans(tf_main, posHide_down, Vector3.one, 0f, Ease.InBounce);

        }

    }
}

