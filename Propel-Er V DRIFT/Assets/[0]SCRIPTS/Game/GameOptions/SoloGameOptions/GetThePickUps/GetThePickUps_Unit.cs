﻿


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class GetThePickUps_Unit : MonoBehaviour {



    GetThePickUps_Mng GM;
    public Collider cldr;
    public Transform tf_miniMapRenderer;

    private void Start()
    {
        GM = GetThePickUps_Mng.ins;
        GM.units.Add(this);
        tf_miniMapRenderer.localPosition = Vector3.zero;
    }



	private void OnTriggerEnter(Collider other)
    {
        int playerInt = 0;
        if (other.CompareTag("Player1"))
        {
            playerInt = 1;
        }
        if (other.CompareTag("Player2"))
        {
            playerInt = 2;
        }

        if (playerInt != 0)
        {
            print("RUN :: Coin picked up by player " + name.ToString());
            //send player int and coin value. 
            GM.units.Remove(this);
            GM.unitPickedUp(playerInt);
            animatePickUp();
        }
        else
        {

        }

    }

    void animatePickUp()
    {
        cldr.enabled = false;
        float y = transform.position.y + 10;
        transform.DOMoveY(y, 0.25f, false).SetEase(Ease.InSine).OnComplete(end);
    }

    void end()
    {
        Destroy(gameObject);
    }

}
