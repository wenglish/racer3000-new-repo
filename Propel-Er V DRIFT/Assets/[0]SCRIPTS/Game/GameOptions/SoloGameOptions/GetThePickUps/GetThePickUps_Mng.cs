﻿#region
/// <summary>
/// this is instatnitatd by the Game Mng when LoadingMng.SoloGameMode = 2;
/// there fore we must handle all iniation here, and run back to Game Mng to Start CountDown.
/// </summary>
#endregion





using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;



public class GetThePickUps_Mng : MonoBehaviour {


    /// <summary>
    /// The ins.
    /// </summary>
    public static GetThePickUps_Mng ins;
    [HideInInspector] public List<GetThePickUps_Unit> units;
    //cache at colection of local, use at ers.
    int count_collectedUnitsForLocalPlayer;

    //this list is set via the Track_Mng.trackUnit, at that, start().
    //this array holds empty transforms, this will set each unit to a single pos.
    //by doing so, we can use the same map over yet have new positions for units each time. 
    [HideInInspector] public List<Transform> tf_POTENTIAL_pickUpPoints;


    public Transform tf_InstrucitonsWindow;
    [HideInInspector] public Vector3 posHide_up = new Vector3(0, 45, 0);
    [HideInInspector] public Vector3 posHide_down = new Vector3(0, -15, 0);
    float transitionTime;





	private void Awake(){
        ins = this;
	}



	private void Start(){
        print("MUST SHOW INSTRUCTION WINOW");
        transitionTime = LoadingMng.transitionTime;
        ///
	}

    public void bp_closeInstructionsWindow(){
        runTrans(tf_InstrucitonsWindow, posHide_down, Vector3.one, 0, Ease.InBounce);
        Invoke("StartGamePlay", 1);
    }
    void StartGamePlay(){
        Game_Mng.ins.startCountDown();
    }

    public void runTrans(Transform tf, Vector3 pos, Vector3 scl, float delay, Ease ease)
    {
        tf.DOMove(pos, transitionTime, false).SetEase(ease).SetDelay(delay);
        tf.DOScale(scl, transitionTime).SetEase(ease).SetDelay(delay);
    }





	public void unitPickedUp(int playerInt){
        
        //only sort if collected by the local player. 


        int playerLocal = LoadingMng.localPlayerInt;
        if(playerInt != LoadingMng.localPlayerInt){
            return;
        }else if(playerInt == playerLocal){
            ///add to the colleciton. use in ERS
            count_collectedUnitsForLocalPlayer++;
        }



        ///if units.Count = 0, end match. 
        if(units.Count == 0){
            print("ALL UNITS COLLECTED!!! ");
            print("must bring in ers custom");
            bringInERS();
        }



    }




    void bringInERS(){


        Game_Mng.isActive = false;

        print("bring in ERS. ");
    }











}
