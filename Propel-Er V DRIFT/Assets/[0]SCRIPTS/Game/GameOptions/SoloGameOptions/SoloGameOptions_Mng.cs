﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;




namespace Com.YahManGames.P3K
{
    public class SoloGameOptions_Mng : MonoBehaviour
    {



        public static SoloGameOptions_Mng ins;
        public static int gameMode;


 
        public Transform tf_soloOptions;



        Vector3 posHide_up = new Vector3(0, 45, 0);
        Vector3 posHide_down = new Vector3(0, -15, 0);

        float transitionTime; 



        private void Awake()
        {
            ins = this;

        }


        // Use this for initialization
        void Start()
        {
            transitionTime = LoadingMng.transitionTime;
            tf_soloOptions.position = posHide_down;


        }


        public void bringIn(){


            runTrans(tf_soloOptions, Vector3.zero, Vector3.one, 0.25f, Ease.OutBounce);



        }



        //with no text
        void runTrans(Transform tf, Vector3 pos, Vector3 scl, float delay, Ease ease)
        {
            tf.DOMove(pos, transitionTime, false).SetEase(ease).SetDelay(delay);
            tf.DOScale(scl, transitionTime).SetEase(ease).SetDelay(delay);
        }



  






        public void bp_BeatTheClock()
        {
            LoadingMng.subGameModeInt = 1;
            loadGame();
        }

        public void bp_GetThePickUps()
        {
            LoadingMng.subGameModeInt = 2;
            loadGame();

        }

        public void bp_Practive()
        {
            LoadingMng.subGameModeInt = 3;
            loadGame();
        }



        public void bp_back(){
            bringOut();
            GameOptions_Mng.ins.bringIn();
        }

        public void bringOut()
        {
            runTrans(tf_soloOptions, posHide_down, Vector3.one, 0f, Ease.InBounce);

        }


        int levelToLoad;
        void loadGame(){


            ///must animate in loading scene. 
            LoadingMng.ins.runInCurtain(0.25f);

            //must load level. 
            levelToLoad = 1;
            Invoke("loadLevel", 2);

        }


        void loadLevel()
        {

            SceneManager.LoadScene(levelToLoad);


        }



	}

}
