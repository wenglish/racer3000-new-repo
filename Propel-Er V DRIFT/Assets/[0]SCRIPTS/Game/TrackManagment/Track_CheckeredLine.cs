﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track_CheckeredLine : MonoBehaviour {


    //assigned by the TM at start. 
    [HideInInspector]public Track_Unit TM;


    //set via the Track Unit, if all check points checked (when player correclty navigayes course)
    [HideInInspector] public bool isActive_P1;
    [HideInInspector] public bool isActive_P2;

	// Use this for initialization
	void Start () {

        isActive_P1 = true;
        isActive_P2 = true;
	}



	private void OnTriggerEnter(Collider other)
	{


        print("CHEKERED ENTER: " + other.gameObject.name);
        if(other.CompareTag("Player1")){

            if(isActive_P1){

                isActive_P1 = false;
                Track_Mng.ins.lapCompleted(1);

            }

        }
        if(other.CompareTag("Player2")){

            if (isActive_P2)
            {
                
                isActive_P2 = false;
                Track_Mng.ins.lapCompleted(2);


            }
        }


	}






}
