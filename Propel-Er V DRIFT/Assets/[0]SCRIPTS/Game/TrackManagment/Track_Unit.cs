﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track_Unit : MonoBehaviour {

    Track_Mng TM;




    public Track_CheckeredLine checkeredLine;
    public Transform tf_startPointP1;
    public Transform tf_startPointP2;


    public List<Track_CheckPoint_Unit> checkPoints;
    int count_checkPointsMade_P1;
    int count_checkPointsMade_P2;
    public Color[] c_checkPoint;

    ///apply in multi. and beat the clock... !! as to how we set this value is yet to be determined.  
    [HideInInspector]public int lapsRequired;



    /// <summary>
    /// this are used by coin_mng and or get the pick ups mng (if active)
    /// ...set the pickups to the positions here. 
    /// ...select randomly and then remove from list, so no repeats.
    /// </summary>
    public List<Transform> tf_potenial_pickUpPoints;





	// Use this for initialization
	void Start () {


        TM = Track_Mng.ins;
        TM.track_Unit = this;
        checkeredLine.TM = this;
        TM.lapsRequired = lapsRequired;


        for (int i = 0; i < checkPoints.Count; i++){

            ///responsibility of the level builder to insure checkPoints are placed by hand into correct order. 
            /// //use as order player must travel, prevetns cheating.
            checkPoints[i].setUnit(this, i);
            //checkPoints[i].tmp_miniMapDisplay.faceColor = c_checkPoint[0];
            int checkRankDisplay = i + 1;
            //checkPoints[i].tmp_miniMapDisplay.text = checkRankDisplay.ToString();
        }



        //get the local player int
        int localPlayerInt = LoadingMng.localPlayerInt;


	}


    public void checkPointMade(int playerInt, int checkRank){

        print("CHECK POINT MADE.. check if order is correct. ");
        if(playerInt == 1){

            print("check player 1");
            if(checkRank == count_checkPointsMade_P1){


                print("rank of check == check points made.... return true " + count_checkPointsMade_P1);

                ///animat the check point. 
                checkPoints[count_checkPointsMade_P1].animateMade();

                //check if laps completed. if rank = 0;
                if(checkRank == 0){
                    TM.lapCompleted(playerInt);
                }
    


                //add up and check significance of check point. 
                count_checkPointsMade_P1++;
                if(count_checkPointsMade_P1 == checkPoints.Count){


                    checkeredLine.isActive_P1 = true;
                    count_checkPointsMade_P1 = 0;
                    //the checkerline will handle line crossing, making the lap official. 
                    //as the player is only at last turn. 
                    return;
                }else{


                    checkeredLine.isActive_P1 = false;
                }
            }else{
                //print("check point hit, out of order. Send Alert. !!");
            }


        }

        if (playerInt == 2)
        {
            if (checkRank == count_checkPointsMade_P2)
            {

                count_checkPointsMade_P2++;
                if (count_checkPointsMade_P2 == checkPoints.Count)
                {
                    checkeredLine.isActive_P2 = true;
                    count_checkPointsMade_P2 = 0;
                    //the checkerline will handle line crossing, making the lap official. 
                    //as the player is only at last turn. 
                    return;
                }
                else{
                    checkeredLine.isActive_P2 = false;
                }

            }


        }


    }




    /// <summary>
    /// sent via the Track Checkered Line if is active on player x. 
    /// //is active if all check points crossed.
    /// </summary>
    public void lapCompleted(int playerInt){






    }



    public void resetAllCheckPoints(){


        ///reset all check points. 
        for (int i = 0; i < checkPoints.Count; i++)
        {
            checkPoints[i].animateReset();

        }

    }




}
