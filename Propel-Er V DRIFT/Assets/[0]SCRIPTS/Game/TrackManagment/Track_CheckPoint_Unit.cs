﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;



public class Track_CheckPoint_Unit : MonoBehaviour {

    [HideInInspector]public Track_Unit track_Unit;


    //sent via the Track Unit at Start. Use to check if order of checks made by player is correct.
    [HideInInspector]public int checkRank;


    public TextMeshPro[] tmp_units;

    public Transform tf_unit;

    public SpriteRenderer[] sp_rndrs;



	// Use this for initialization
	void Start () {
		


	}

    public void setUnit(Track_Unit unit, int unitRank){

        track_Unit = unit;
        checkRank = unitRank;

        int displayRank = checkRank + 1;
        for (int i = 0; i < tmp_units.Length; i++){
            tmp_units[i].text = displayRank.ToString();
        }




    }



	private void OnTriggerEnter(Collider other)
	{
        int playerInt = 0;
        if(other.CompareTag("Player1")){

            playerInt = 1;

        }

        if (other.CompareTag("Player2"))
        {
            playerInt = 2;

        }

        if(playerInt != 0){
            print("P"+playerInt+" Crossed " + transform.name);
            track_Unit.checkPointMade(playerInt, checkRank);
 
        }
	}








    public void animateMade(){


        Vector3 trgtScale = new Vector3(1, 0, 1);
        Vector3 trgtPos = new Vector3(tf_unit.position.x, tf_unit.position.y + 20, tf_unit.position.z);

        tf_unit.DOScale(trgtScale, 1);
        tf_unit.DOMove(trgtPos, 1, false);


        Color trgtC = track_Unit.c_checkPoint[1];
        for (int i = 0; i < sp_rndrs.Length; i++){

            sp_rndrs[i].DOColor(trgtC, 1);

        }





    }


    //bool used at first check point. 
    bool canReset;
    public void animateReset(){


        if (!canReset){
            canReset = true;
            return;
        }
        else
        {

            Vector3 trgtScale = new Vector3(1, 1, 1);
            Vector3 trgtPos = new Vector3(tf_unit.position.x, tf_unit.position.y - 20, tf_unit.position.z);

            tf_unit.DOScale(trgtScale, 1);
            tf_unit.DOMove(trgtPos, 1, false);


            Color trgtC = track_Unit.c_checkPoint[0];
            for (int i = 0; i < sp_rndrs.Length; i++)
            {

                sp_rndrs[i].DOColor(trgtC, 1);

            }

        }
    }




}
