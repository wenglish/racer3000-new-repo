﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;



public class CameraFollow : MonoBehaviour
{
    public static CameraFollow ins;
    public Transform tf_player;
    public Camera cam;

    public Vector3 pos_atRaceEnd = new Vector3(-0.97f, 2.7f, -22.3f);//this cam parented to player (local).
    Vector3 rtn_atRaceEnd = new Vector3(11.4f,0,0);


	private void Awake()
	{
        ins = this;
        transform.position = Vector3.zero;
	}

	// Use this for initialization
	void Start()
    {

        Transform tf_start = null;
        if(LoadingMng.localPlayerInt == 1){

            tf_start = Track_Mng.ins.track_Unit.tf_startPointP1;

        }
        if (LoadingMng.localPlayerInt == 2)
        {

            tf_start = Track_Mng.ins.track_Unit.tf_startPointP2;

        }

        //transform.position = tf_start.position;
        //transform.rotation = tf_start.rotation;


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (tf_player != null){
            if (Game_Mng.isActive)
            {
                update_pos();
                update_rtn();
            }else{


            }
        }
    }

    void update_pos(){
        Vector3 playerPos = tf_player.position;
        float step = Time.deltaTime * 5;
        transform.position = Vector3.Lerp(transform.position, playerPos, step);


    }
    void update_rtn(){
        Quaternion playerRtn = tf_player.rotation;
        float step = Time.deltaTime * 2;
        transform.rotation = Quaternion.Lerp(transform.rotation, playerRtn, step);
    }




    public void setCamToPlayer_atRaceEnd(){
        transform.parent = tf_player;
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = new Vector3(0, 225, 0);
        cam.transform.localPosition = pos_atRaceEnd;
        cam.transform.localEulerAngles = rtn_atRaceEnd;
        float y = 225 - 360;
        Vector3 rtn_end = new Vector3(0, y, 0);
        transform.DOLocalRotate(rtn_end, 8, RotateMode.FastBeyond360).SetLoops(-1).SetEase(Ease.Linear);
    }

   


}
