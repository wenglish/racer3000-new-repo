﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapManager : MonoBehaviour {

    public static LapManager ins;
    int lapCount_P1;
    int lapCount_P2;
    public int lapCount_toWin;
    bool isRaceWon;

    private void Awake(){


        ins = this;


	}


	// Use this for initialization
	void Start () {
		
	}


    public void lapCompleted(int playerInt)
    {

        if (isRaceWon)
        {

        }
        else
        {
            if (playerInt == 1)
            {
                lapCount_P1++;
                AchievementsManager.lapsCompleted++;
                if (lapCount_P1 == lapCount_toWin){
                    AchievementsManager.racesWon++;
                    raceWon(1);
                }
            }
            if (playerInt == 2)
            {
                lapCount_P2++;
                if (lapCount_P2 == lapCount_toWin){
                    AchievementsManager.racesLost++;
                    raceWon(2);
                }
            }
            print("LapCount P1" + lapCount_P1);
        }
    }

    void raceWon(int playerInt){
        isRaceWon = true;
        print("P" + playerInt + " HAS WON THE RACE");
    }


}
