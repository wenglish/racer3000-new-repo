﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Photon;

public class Game_Mng : PunBehaviour {


    public static Game_Mng ins;
    Coin_Mng coinMng;
    int gameModeInt;

    public GameObject pf_player;
    [HideInInspector] public PlayerUnit playerLocal;
    [HideInInspector] public PlayerUnit Player1;
    [HideInInspector] public PlayerUnit Player2;

    public static bool isActive;


    public GameObject pf_GAMEMODE_BeatTheClock;//1
    public GameObject pf_GAMEMODE_CollectAllPickUps;//2
    public GameObject pf_GAMEMODE_Practice;//3

    public PhotonView photonView;










    void Awake(){

        if (ins == null)
        {
            ins = this;
            coinMng = Coin_Mng.ins;

        }
        else{
            Destroy(gameObject);
        }
    }


	private void Start(){


        gameModeInt = LoadingMng.gameModeInt;

        if(gameModeInt == 1){
            Create_NonNetWorkPlayer();

        }else if (gameModeInt == 2){
            //since we are in mulit, Destory default player 1 and two, instantiate via network (each client does this, locally).
            Create_NetworkPlayer();

        }

        //check which sub game mode int is active.
        int subGameModeInt = LoadingMng.subGameModeInt;
        if (subGameModeInt == 1){
            //beat the clock.
            isUseLaps = true;
        }
        if (subGameModeInt == 2){
            //collect all pick ups. 
            isUseLaps = false;
        }

        if (subGameModeInt == 3){
            //practice
            isUseLaps = false;
        }




       //
	}


    #region creat non network player. 
    void Create_NonNetWorkPlayer(){
        
        GameObject gm = Instantiate(pf_player, transform);
    }


    ////
    /// in the case of is online multi play, this has destroyed the native player 1 and player 2, and each client has created its own player over the network.
    /// at that point, in that PlayerUnit, at Start(), we report back here. 
    /// this method will sort player ints, and set tags.
    public void local_PlayerCreated(PlayerUnit playerUnit)
    {
        playerLocal = playerUnit;
        Player1 = playerLocal;
        Vector3 pos = new Vector3(0, 1.5f, 0);
        Player1.transform.position = pos;
        Player1.transform.rotation = Track_Mng.ins.track_Unit.tf_startPointP1.rotation;
        //MiniMap_Mng.ins.tf_player = playerUnit.playerCntrlr.tf_player;
        CameraFollow.ins.tf_player = playerUnit.playerCntrlr.tf_player;
        playerUnit.playerCntrlr.gameObject.tag = "Player1";
        Invoke("setLocalPlayer", 0.125f);
        LoadingMng.ins.runOutCurtain(1f);

        if(LoadingMng.isTutorial){
            Tutorial_Mng.ins.run_driveTutorial(playerUnit);
            countDownMng.transform.localScale = Vector3.zero;
        }
        else{
            Invoke("startCountDown", 2);
        }



    }
    #endregion



    #region Create Network Player 
    void Create_NetworkPlayer(){
        print("CREATE PLAYRES ON NETWORk.");
        //instantiate over network, this local player
        Transform tf = null;
        if(LoadingMng.localPlayerInt == 1){
            print(" set tf for 1. ");
            tf = Track_Mng.ins.track_Unit.tf_startPointP1;
        }
        if(LoadingMng.localPlayerInt == 2){
            print(" set tf for 2 ");
            tf = Track_Mng.ins.track_Unit.tf_startPointP2;
        }

        if (tf == null){
            print("WHY IS TF NULL.");
            print("LOADING MNG LOCAL PLAYER INT = " + LoadingMng.localPlayerInt);
        }
        else{
            Vector3 startPos = tf.position;
            Quaternion rtn = tf.rotation;
            GameObject gm = PhotonNetwork.Instantiate("PlayerShipXX", startPos, rtn, 0);
            ///this player will report back here at its own start()
        }
    }


    ////
    /// in the case of is online multi play, this has destroyed the native player 1 and player 2, and each client has created its own player over the network.
    /// at that point, in that PlayerUnit, at Start(), we report back here. 
    /// this method will sort player ints, and set tags.
    public void nr_PlayerCreated(PlayerUnit playerUnit){


        print("nr playuers created. CREATE PLAYRES ON NETWORk.");
        if(playerUnit.photonView.isMine){
            playerUnit.playerInt = LoadingMng.localPlayerInt;
            playerLocal = playerUnit;
            if(LoadingMng.localPlayerInt == 1){
                playerUnit.playerCntrlr.gameObject.tag = "Player1";
                playerUnit.gameObject.name = "Player 1 Local";
                Player1 = playerUnit;
                Player1.transform.position = Track_Mng.ins.track_Unit.tf_startPointP1.position;
                Player1.transform.rotation = Track_Mng.ins.track_Unit.tf_startPointP1.rotation;

            }
            if(LoadingMng.localPlayerInt == 2){
                playerUnit.playerCntrlr.gameObject.tag = "Player2";
                playerUnit.gameObject.name = "Player 2 Local";
                Player2 = playerUnit;
                Player2.transform.position = Track_Mng.ins.track_Unit.tf_startPointP2.position;
                Player2.transform.rotation = Track_Mng.ins.track_Unit.tf_startPointP2.rotation;
            }
            //MiniMap_Mng.ins.tf_player = playerUnit.playerCntrlr.tf_player;
            CameraFollow.ins.tf_player = playerUnit.playerCntrlr.tf_player;
            print("local player set ");




            Invoke("setLocalPlayer", 0.125f);
            LoadingMng.ins.runOutCurtain(1f);
            Invoke("startCountDown", 2);



        }else{

            int localPlayerInt = LoadingMng.localPlayerInt;
            int remotePlayerInt = 0;
            if(localPlayerInt == 1){
                remotePlayerInt = 2;
                playerUnit.playerCntrlr.gameObject.tag = "Player2";
                playerUnit.gameObject.name = "Player 2 Remote";
            }
            if(localPlayerInt == 2){
                remotePlayerInt = 1;
                playerUnit.playerCntrlr.gameObject.tag = "Player1";
                playerUnit.gameObject.name = "Player 1 Remote";
            }

            playerUnit.playerInt = remotePlayerInt;




        }



        if (Player1 != null && Player2 != null)
        {

            ///init start game.
            LoadingMng.ins.runOutCurtain(1f);
            Invoke("startCountDown", 2);
        }
   
        
    }
    #endregion








    /// <summary>
    /// sent from this.Start() we check the game mode and local player int, cahced in the Loading mng.
    /// we then compare the local player value and gamemodes to determine the localPlayerUnit. 
    /// </summary>

    public delegate void del_localPlayerSet(PlayerUnit playerUnit);
    public event del_localPlayerSet evt_localPlayerSet;

    void setLocalPlayer(){

        print("game mng . local player set. ");

        ///various magnaers, are subscribed to this, ex. Coin Mng. 

        if (evt_localPlayerSet != null)
        {
            evt_localPlayerSet(playerLocal);
        }

    }


    public CountDown_Mng countDownMng;
    public void startCountDown(){
         countDownMng.initCountDown();
    }





    public void setActive(bool activeStatus){
        isActive = activeStatus;
    }










    bool isUseLaps;//set at start by gamemodeint. 
    bool isRaceWon;
    public void lapsCompleted(int playerInt){

        print("GAME MNG LAPS COMPLETED");

        //check if gamemodeint is for laps use. 
        //set via subGameModeInt. If == 1. isUSeLaps = true.
        if (!isUseLaps){
            print("RETURN. LAPS NOT IN USE");
            return;
        }
        else{
            if(isRaceWon){
                print("return, race is won");
                return;
            }
            else{
                isRaceWon = true;
                playerWon(playerInt);
            }
        }

    }
    void playerWon(int playerInt){
        print("PLAYER " + playerInt + " WON");
        if(gameModeInt == 1){
            //solo game play. do not worry about networking.
            isActive = false;
            nr_playerWon(1);

        }else if(gameModeInt == 2){
            //is mulit. sort networking
            photonView.RPC("nr_playerWon", PhotonTargets.AllViaServer, playerInt);
        }

       
    }
    [PunRPC]
    void nr_playerWon(int playerWonInt){
        //sent via this, over network, in case of multi player.

        if(playerWonInt == LoadingMng.localPlayerInt){
            ERS_Mng.ins.raceOver(1);
        }else{
            ERS_Mng.ins.raceOver(2);
        }

        CameraFollow.ins.setCamToPlayer_atRaceEnd();
        //MiniMap_Mng.ins.gameObject.SetActive(false);
        playerLocal.ui_Mng.gameObject.SetActive(false);

    }



    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
    }




}
