﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;


public class ERS_Mng : MonoBehaviour {



    public static ERS_Mng ins;
    public Transform tf_main;
    Vector3 hidePos = new Vector3 (0, -18.4f, -5.9f); 
    int gameMode;
    int localPlayerInt;
    public Camera cam;
    float transitionTime;
    bool isActive;
    public TextMeshPro tmp_winStatus;


    private void Awake(){
        ins = this;
    }

	// Use this for initialization
	void Start () {
        gameMode = LoadingMng.gameModeInt;
        localPlayerInt = LoadingMng.localPlayerInt;
        tf_main.position = hidePos;
        cam.enabled = false;
        transitionTime = LoadingMng.transitionTime;
	}




    public void raceOver(int winStatus){

        if(winStatus == 1){
            //player won
            tmp_winStatus.text = "WON!";
        }
        else if (winStatus == 2){
            //player lost
            tmp_winStatus.text = "LOST";
        }




        cam.enabled = true;
        tf_main.DOMove(Vector3.zero, transitionTime, false).SetEase(Ease.OutBounce).OnComplete(activate);
    }

    void activate(){
        isActive = true;
    }









    public void bp_playAgain(){
        print("bp play again! ");
        if(!isActive){
            
        }
        else{
            print("bp play commit. play again! ");
            isActive = false;
            LoadingMng.ins.runInCurtain(0.25f);
            LoadingMng.ins.init_sceneChange(0, 0.25f);
        }

    }
    public void bp_home(){
        if (!isActive)
        {

        }
        else
        {
            isActive = false;
            LoadingMng.ins.runInCurtain(0.25f);
            LoadingMng.ins.init_sceneChange(0, 0.25f);
        }
    }
    public void bp_store(){
        if (!isActive)
        {

        }
        else
        {
            isActive = false;
            LoadingMng.ins.runInCurtain(0.25f);
            LoadingMng.ins.init_sceneChange(0, 0.25f);
        }
    }
    public void bp_stats(){
        if (!isActive)
        {

        }
        else
        {
            isActive = false;
            LoadingMng.ins.runInCurtain(0.25f);
            LoadingMng.ins.init_sceneChange(0, 0.25f);
        }
    }




}
