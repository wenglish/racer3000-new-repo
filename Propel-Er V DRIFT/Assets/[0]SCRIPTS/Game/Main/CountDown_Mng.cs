﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Photon;

public class CountDown_Mng : PunBehaviour {


    public Transform tf_countdown;
    public SpriteRenderer[] sp_countdowns;
    public Color[] colors;
    int countCountDown;
    public Camera camera;
    public PhotonView photonView;

    public void initCountDown(){
        if (LoadingMng.gameModeInt == 1)
        {
            countCountDown = 0;
            InvokeRepeating("changeLights", 1, 1);
        }
        else if (LoadingMng.gameModeInt == 2){
            if(PhotonNetwork.isMasterClient){
                InvokeRepeating("ns_changeLights", 1, 1);
            }
        }

    }
    //only recieved by the master client, repeatinv via invoke.
    void ns_changeLights(){

        photonView.RPC("changeLights", PhotonTargets.AllViaServer);

    }

    [PunRPC]
    void changeLights(){


        if (countCountDown == 3)
        {
            CancelInvoke("changeLights");
            tf_countdown.DOMoveX(5, 0.25f, false).SetEase(Ease.InBounce).OnComplete(turnOffCam);
            Game_Mng.ins.setActive(true);
            return;
        }



        for (int i = 0; i < sp_countdowns.Length; i++){

            sp_countdowns[i].color = colors[0];

        }




        sp_countdowns[countCountDown].color = colors[countCountDown + 1];

        print("CHANGE LIGHTS " + countCountDown);


        countCountDown++;

 

    }


    void turnOffCam(){
        camera.enabled = false;
    }


    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
    }



}
