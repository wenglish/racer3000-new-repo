﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile_A : MonoBehaviour {



    public PlayerCntrlr playerMng;

    public Rigidbody2D rgby_head;
    public Transform[] tf_tracers;
    int tracerCount;
    [HideInInspector]public Vector2 force_vector; 



  

	// Use this for initialization
	void Start () {
       
        //playerMng.evt_shootProjectile += shoot;


    }

	public void reset(){
        transform.parent = playerMng.transform;
        transform.localPosition = Vector3.zero;
        rgby_head.transform.localPosition = Vector3.zero;
        rgby_head.velocity = Vector2.zero;
        rgby_head.transform.localScale = Vector3.zero;
        tracerCount = 0;
        StopAllCoroutines();

        for (int i = 0; i < tf_tracers.Length; i++){
            tf_tracers[i].localScale = Vector2.zero;
        }
	}


	void shoot(float power){


        //must shoot projectile down in local space. 

        //tracer dots must appeaar every so often. 

        //projectile must cover disantce of x, at ehich point it is destroyed and reset. 

        ///distance of x, is dependant on power of playerCntlr shot power. 


        rgby_head.velocity = Vector2.zero;

       

        float x = ((force_vector.x) * power)/2;
        float y = ((force_vector.y) * power)/2;
        force_vector = new Vector2(x, y) * -1;

        transform.parent = transform.parent.parent;
        transform.localScale = Vector3.one;
        rgby_head.transform.localScale = Vector3.one;
        rgby_head.AddForce(force_vector, ForceMode2D.Impulse);


        StartCoroutine(leaveTracers());
    }

    IEnumerator leaveTracers(){
        bool isActive = true;

        Vector2 startPos = playerMng.transform.position;



        while (isActive){
            Vector2 crntPos = playerMng.transform.position;
            float d = Vector2.Distance(startPos, crntPos);
            if(d > 1f){
                startPos = playerMng.transform.position;
                tf_tracers[tracerCount].position = startPos;
                tf_tracers[tracerCount].localScale = Vector3.one;
                tracerCount++;
                tf_tracers[tracerCount].position = rgby_head.transform.position;
                tracerCount++;
            }


            yield return null;
        }




       
    }
	
}
