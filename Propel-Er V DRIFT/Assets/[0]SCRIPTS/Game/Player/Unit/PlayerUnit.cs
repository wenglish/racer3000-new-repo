﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;


public class PlayerUnit : PunBehaviour {

    Game_Mng GM;
    [HideInInspector] public int playerInt;
    public PlayerCntrlr playerCntrlr;
    public CoinHUD_Mng coinHUD_Mng;
    public PhotonView photonView;
    public PlayerUI_Mng ui_Mng;


	// Use this for initialization
	void Start () {
        print("PLAYER UNIT. START()");
        GM = Game_Mng.ins;
        playerCntrlr.playerUnit = this;
        //GM.evt_localPlayerSet += localPlayerSet;
        //coinHUD_Mng.playerUnit = this;


        //
        if(LoadingMng.gameModeInt == 1){

            GM.local_PlayerCreated(this);

        }


        //check if this is online, multi. 
        if(LoadingMng.gameModeInt == 2){
            print("is online multi . START()");
            //then we must report back the game mng. 
            GM.nr_PlayerCreated(this);
            if(photonView.isMine){
                playerCntrlr.isLocal = true;
            }else{
                ui_Mng.gameObject.SetActive(false);
            }
        }
	}



    void localPlayerSet(PlayerUnit playerUnit_del){
        
        if(playerUnit_del == this){
            playerInt = LoadingMng.localPlayerInt;
            playerCntrlr.rgby.useGravity = true;


            //playerCntrlr.tf_player.position = Track_Mng.ins.track_Unit.tf_startPoint.position;
            //playerCntrlr.tf_player.localEulerAngles = Track_Mng.ins.track_Unit.tf_startPoint.localEulerAngles;
            MiniMap_Mng.ins.tf_player = playerCntrlr.tf_player;
        }else{
            print("this player unit is not on the local player. ");

        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
    }



}
