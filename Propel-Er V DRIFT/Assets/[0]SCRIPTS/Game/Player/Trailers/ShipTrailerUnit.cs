﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipTrailerUnit : MonoBehaviour {

    public PlayerUnit playerUnit;
    PlayerCntrlr playerCntrlr;
    Transform playerTransform;
    Rigidbody rgby;

    //public LineRenderer lineRenderer;
    public TrailRenderer trailRenderer;

    float timeIntervals = 0.25f;
    bool isWatching;
    float timeSet;


    int setCount;



    // Use this for initialization
	void Start () {


        playerCntrlr = playerUnit.playerCntrlr;
        playerTransform = playerCntrlr.tf_player;
        rgby = playerCntrlr.rgby;

	}


    private void Update()
    {

        if (rgby != null)
        {
            //base length of trail via rgby velocity. 
            float velMag = rgby.velocity.magnitude;
            print("VEL MAG = " + velMag);
        }





        /*unit whcih uses Line Renderer Method. 
        if(playerTransform != null){

            float timeCurrnt = Time.time;
            lineRenderer.SetPosition(0, playerTransform.position);
            if(!isWatching){
                timeSet = timeCurrnt + timeIntervals;
                isWatching = true;
                return;
            }else{
                
                if(timeCurrnt >= timeSet){

                    lineRenderer.SetPosition(setCount, playerTransform.position);
                    setCount++;
                    if(setCount >= lineRenderer.positionCount){
                        setCount = 1;
                    }
                    isWatching = false;
                    return;
                }

            }

        }*/

    }

}
