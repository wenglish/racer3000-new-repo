﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;



public class PlayerCntrlr : MonoBehaviour {


    [HideInInspector]public Transform tf_player;
    [HideInInspector] public PlayerUnit playerUnit;
    public Camera cam;
    public Rigidbody rgby;
    Vector3 force_vector;
    public float power;
    public float clampVelocity = 20;
    public float time_deltaFctr;
    float midScreen;
    [HideInInspector]public bool isLocal;


	// Use this for initialization
	void Start () {
        tf_player = transform;
        midScreen = Screen.width/2;
        rgby.useGravity = false;
	}

    // Update is called once per frame
    void Update()
    {
        if(!Game_Mng.isActive){
            return;
        }



        if (LoadingMng.gameModeInt == 1)
        {

            if (Input.GetMouseButtonDown(0))
            {
                StopAllCoroutines();
                initDrag = rgby.drag;
                initAngDrag = rgby.angularDrag;
                last_mouse = Input.mousePosition;
            }


            if (Input.GetMouseButton(0))
            {


                getAngle(Input.mousePosition);
                addDrag();


            }


            if (Input.GetMouseButtonUp(0))
            {
                StopAllCoroutines();
                StartCoroutine(subDrag());
            }

        }

        if (LoadingMng.gameModeInt == 2 && isLocal)
        {

            if (Input.GetMouseButtonDown(0))
            {
                StopAllCoroutines();
                initDrag = rgby.drag;
                initAngDrag = rgby.angularDrag;
                last_mouse = Input.mousePosition;
            }


            if (Input.GetMouseButton(0))
            {


                getAngle(Input.mousePosition);
                addDrag();


            }


            if (Input.GetMouseButtonUp(0))
            {
                StopAllCoroutines();
                StartCoroutine(subDrag());
            }

        }



	}






	private void FixedUpdate()
    {
        if (!Game_Mng.isActive){
            return;
        }
        else{
            hoverBody();
            moveBody();
        }
	}



	void resetPlayer(){
        //print("==== RESET PLAYER ======= ");
        rgby.velocity = new Vector2(0, 0);
        //power = 0;
    }


    public float hoverHeight = 5;
    public float hoverForce = 100;
    bool isOnTrack;
    void hoverBody(){

        Ray ray = new Ray(tf_player.position, -transform.up);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, hoverHeight)){

            float proportionalHieght = (hoverHeight - hit.distance) / hoverHeight;
            Vector3 appliedHoverForce = (Vector3.up * 10);
            rgby.AddForce(appliedHoverForce, ForceMode.Acceleration);
            ////print("appliedHoverForce = " + appliedHoverForce);

            if(hit.collider.CompareTag("onTrack")){

                if(isOnTrack){
                    
                }
                else{
                    isOnTrack = true;
                }
            }
            if (hit.collider.CompareTag("offTrack"))
            {
                if (!isOnTrack)
                {

                }
                else
                {
                    isOnTrack = false;
                }

            }
        }



    }






 
    Vector2 init_tap;
    Vector2 delta_mouse;
    Vector2 last_mouse;
    public float additiveSpinValue;
    Vector2 vel;
    float initDrag;
    float initAngDrag;

    void getAngle(Vector2 mouse_pos)
    {

       // print("--===== get angle ======-- ");
      

        if(mouse_pos.y >= Screen.height){
            return;
        }
  
 



        delta_mouse = mouse_pos - last_mouse;
        float delta_mag = delta_mouse.magnitude;
        //print("delta mag " + delta_mag);

  

        if(Mathf.Approximately(last_mouse.x, mouse_pos.x) && Mathf.Approximately(last_mouse.y, mouse_pos.y)){

            return;

        }

        last_mouse = mouse_pos;





        if(Mathf.Abs(delta_mouse.x) > Mathf.Abs(delta_mouse.y)){
            //check x for movement. 

            if(delta_mouse.x < 0){
                //spin right
                //print("r");
                spinRight(delta_mag);
         

            }else{
                //spin left
               // print("l");
                spinLeft(delta_mag);
     
            }

        }

        /*
        if (Mathf.Abs(delta_mouse.x) < Mathf.Abs(delta_mouse.y)){
            //check y for movement.
            // cahche x 
            float x_crnt = Input.mousePosition.x;
            if (delta_mouse.y < 0)
            {
                if (x_crnt > midScreen)
                {
                    //spin right
                    spinLeft(delta_mag);
             
                }else{
                    //spin left
                    spinLeft(delta_mag);
           
                }
            }
            else{
                if (x_crnt > midScreen)
                {
                    //spin right
                    spinRight(delta_mag);
     
                }
                else
                {
                    //spin left
                    spinRight(delta_mag);
             
                }

            }

        }*/



    }


    void spinLeft(float delta_mag){
        //print("SL...........");
        float y_new = tf_player.localEulerAngles.z;
        y_new = y_new - (additiveSpinValue + delta_mag);
        Vector3 torque = new Vector3(0, y_new, 0);
        rgby.AddRelativeTorque(torque, ForceMode.Force);

    }
    void spinRight(float delta_mag){
        //print("SR............");
        float y_new = tf_player.localEulerAngles.z;
        y_new = y_new + (additiveSpinValue + delta_mag);
        Vector3 torque = new Vector3(0, y_new, 0);
        rgby.AddRelativeTorque(torque, ForceMode.Force);
    }






    void squeezePlayer(){

        float y = tf_player.localScale.y;
        y -= 0.0125f;
        y = Mathf.Clamp(y, 0.5f, 1);
        tf_player.localScale = new Vector2(1, y);


    }




    void takeShot(){
        moveBody();
        resetScale();
    }




    void moveBody(){

        if (!Game_Mng.isActive){
            rgby.AddForce(Vector3.zero);
            return;
        }
        else
        {

            float power_local = power;

            if (isOnTrack)
            {
                power_local = power;
            }
            else
            {
                power_local = power / 2;
            }

            force_vector = transform.forward * power_local;
            rgby.AddForce(force_vector);
        }

    }

    public float dragValue;
    float drag;
    float angDrag;

    void addDrag(){


        drag = initDrag + (dragValue/2);
        drag = Mathf.Clamp(drag, 0, 10);
        rgby.drag = drag;
        angDrag = initAngDrag + (dragValue * 0.5f);
        angDrag = Mathf.Clamp(angDrag, 0, 35);
        rgby.angularDrag = angDrag;

    

    }

    IEnumerator subDrag()
    {
       ////print("SUB DRAG ");
        bool isAdd = true;

        while (isAdd)
        {
            drag -= (dragValue * 1.5f);
            drag = Mathf.Clamp(drag, 0, 100);
            rgby.drag = drag;
            ////print("- drag = " + drag);

            angDrag -= dragValue;
            angDrag = Mathf.Clamp(angDrag, 7, 100);
            rgby.angularDrag = angDrag;
            ////print("- angDrag = " + angDrag);


            //Vector3 angVel = rgby.angularVelocity/4;
            //rgby.angularVelocity = angVel;
            yield return null;
        }



    }


    void resetScale(){
        //reset scale of player
        tf_player.DOKill();
        tf_player.DOScaleY(1, 0.25f).SetEase(Ease.OutBounce);
    }



    public delegate void del_shootProjectile(float power);
    public event del_shootProjectile evt_shootProjectile;
    void shootProjectile(){
        if(evt_shootProjectile != null){
            evt_shootProjectile(power);
        }
    }

   



 




}
