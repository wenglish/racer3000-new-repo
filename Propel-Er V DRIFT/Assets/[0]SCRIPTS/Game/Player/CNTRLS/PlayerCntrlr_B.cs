﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;



public class PlayerCntrlr_B : MonoBehaviour
{


    Transform tf_player;
    public Camera cam;
    public Rigidbody2D rgby;
    Vector2 force_vector;
    float power;
    public float clampVelocity = 20;
    public float time_deltaFctr;
    public CameraFollow cameraFollow;
    float midScreen;
    bool isNewShot;


    // Use this for initialization
    void Start()
    {
        tf_player = transform;
        midScreen = Screen.width / 2;
        isNewShot = true;
        //Time.timeScale = 0.0125f;
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetMouseButtonDown(0))
        {
            StopAllCoroutines();
            angle_init = 0;
            dir = 0;
            last_mouse = Input.mousePosition;
            StartCoroutine(addDrag());
        }


        if (Input.GetMouseButton(0))
        {
            rgby.drag += 0.1f;
            rgby.angularDrag += 0.1f;
            getAngle(Input.mousePosition);
        }



        if (Input.GetMouseButtonUp(0))
        {
            StopAllCoroutines();
            isNewShot = true;
            StartCoroutine(subDrag());
        }



        moveBody();

    }



    void resetPlayer()
    {
        print("==== RESET PLAYER ======= ");
        rgby.velocity = new Vector2(0, 0);
        power = 0;
    }



    float angle_init;
    Vector2 init_tap;
    Vector2 delta_mouse;
    Vector2 last_mouse;
    int crnt_quad;
    int last_quad;
    int dir;
    public float additiveSpinValue;
    Vector2 vel;


    void getAngle(Vector2 mouse_pos)
    {




        if (mouse_pos.y >= Screen.height)
        {
            print("RETURN . ");
            return;
        }





        delta_mouse = mouse_pos - last_mouse;
        float delta_mag = delta_mouse.magnitude;
        print("delta mouse mag = " + delta_mouse.magnitude);

        last_mouse = mouse_pos;
        if (delta_mouse.sqrMagnitude < 1)
        {
            if (Mathf.Approximately(delta_mouse.x, 0) && Mathf.Approximately(delta_mouse.y, 0))
            {
                if (dir != 0)
                {
                    if (dir == 1)
                    {
                        spinRight(delta_mag);
                    }
                    else
                    {
                        spinLeft(delta_mag);
                    }

                }
            }
            return;
        }


        if (Mathf.Abs(delta_mouse.x) > Mathf.Abs(delta_mouse.y))
        {
            //check x for movement. 

            if (delta_mouse.x < 0)
            {
                //spin right
                spinRight(delta_mag);
                dir = 1;

            }
            else
            {
                //spin left
                spinLeft(delta_mag);
                dir = -1;
            }

        }
        if (Mathf.Abs(delta_mouse.x) < Mathf.Abs(delta_mouse.y))
        {
            //check y for movement. 
            if (delta_mouse.y < 0)
            {
                if (init_tap.x > midScreen)
                {
                    //spin right
                    spinRight(delta_mag);
                    dir = 1;
                }
                else
                {
                    //spin left
                    spinLeft(delta_mag);
                    dir = -1;

                }
            }
            else
            {
                if (init_tap.x < midScreen)
                {
                    //spin left
                    spinLeft(delta_mag);
                    dir = -1;
                }
                else
                {
                    //spin right
                    spinRight(delta_mag);
                    dir = 1;
                }

            }

        }



    }


    void spinLeft(float delta_mag)
    {
        float z_new = tf_player.localEulerAngles.z;
        z_new = z_new + (additiveSpinValue + delta_mag);
        tf_player.rotation = Quaternion.Euler(0, 0, z_new);
        cam.transform.rotation = Quaternion.Euler(0, 0, z_new);

    }
    void spinRight(float delta_mag)
    {
        float z_new = tf_player.localEulerAngles.z;
        z_new = z_new - (additiveSpinValue + delta_mag);
        tf_player.rotation = Quaternion.Euler(0, 0, z_new);
        cam.transform.rotation = Quaternion.Euler(0, 0, z_new);
    }


    void applyRtn(float angle)
    {

        Quaternion q1 = tf_player.rotation;
        Quaternion q2 = new Quaternion(q1.x, q1.y, angle, q1.w);
        //tf_player.rotation = Quaternion.r
        //cameraFollow.transform.rotation = q2;
    }




    void squeezePlayer()
    {

        float y = tf_player.localScale.y;
        y -= 0.0125f;
        y = Mathf.Clamp(y, 0.5f, 1);
        tf_player.localScale = new Vector2(1, y);


    }




    void takeShot()
    {
        moveBody();
        resetScale();
    }




    void moveBody()
    {

        force_vector = transform.up * 5;
        rgby.AddForce(force_vector);
    }

    float drag;
    float angDrag;

    IEnumerator addDrag()
    {

        bool isAdd = true;

        while (isAdd)
        {

            drag += 0.001f;
            drag = Mathf.Clamp(rgby.drag, 0, 5);
            rgby.drag = drag;


            angDrag += 0.4f;
            angDrag = Mathf.Clamp(rgby.drag, 0, 5);
            rgby.angularDrag = angDrag;


            yield return null;
        }

    }

    IEnumerator subDrag()
    {
        print("SUB DRAG ");
        bool isAdd = true;

        while (isAdd)
        {
            drag -= 2f;
            rgby.drag = drag;


            angDrag -= 0.25f;
            rgby.angularDrag = angDrag;

            yield return null;
        }



    }


    void resetScale()
    {
        //reset scale of player
        tf_player.DOKill();
        tf_player.DOScaleY(1, 0.25f).SetEase(Ease.OutBounce);
    }



    public delegate void del_shootProjectile(float power);
    public event del_shootProjectile evt_shootProjectile;
    void shootProjectile()
    {
        if (evt_shootProjectile != null)
        {
            evt_shootProjectile(power);
        }
        print("POWER = " + power);
        ///StartCoroutine(watchDistance());
    }

    /*
    IEnumerator watchDistance(){

        bool isWatching = true;

        while (isWatching)
        {


            float x = rgby.velocity.x;
            float y = rgby.velocity.y;
      
             
          
            if (Mathf.Approximately(0, x) && Mathf.Approximately(0, y))
            {

                snapBack();
                StopAllCoroutines();

            }


            yield return null;
        }
    }*/








}
