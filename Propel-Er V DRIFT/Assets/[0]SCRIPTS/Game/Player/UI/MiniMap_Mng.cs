﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap_Mng : MonoBehaviour {


    public static MiniMap_Mng ins;


    [HideInInspector]public Transform tf_player;
    public Transform tf_cam;

    public Transform tf_pointer;




	private void Awake(){
        ins = this;
	}


    private void Start()
    {
        Game_Mng.ins.evt_localPlayerSet += localPlayerSet;
    }

    void localPlayerSet(PlayerUnit playerUnit){


        print("mini map. local player set. ");
        int localPlayerInt = LoadingMng.localPlayerInt;

        if(localPlayerInt == 1){
            tf_player = Game_Mng.ins.Player1.playerCntrlr.tf_player;
        }
        if(localPlayerInt == 2){
            tf_player = Game_Mng.ins.Player2.playerCntrlr.tf_player;
        }
	}




	// Update is called once per frame
	void Update () {

        if(tf_player == null){
            return;
        }else{
            float y = tf_player.localEulerAngles.y;
            tf_cam.localEulerAngles = new Vector3(0, y, 0); 
        }




	}
}
