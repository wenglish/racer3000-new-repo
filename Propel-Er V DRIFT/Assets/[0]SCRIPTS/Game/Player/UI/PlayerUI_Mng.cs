﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI_Mng : MonoBehaviour {



    public PlayerCntrlr playerCntrlr;
    Rigidbody rgby;
    public Transform[] tf_velocitys;



	private void Start(){

        rgby = playerCntrlr.rgby;




	}




	private void Update(){



        float vel = Mathf.Round(Vector3.Magnitude(rgby.velocity));



        //for now with 8 digits of units for vel rep, each notch = 3++ in vel mag.

       // print("VEL = "+ vel);
        for (int i = 0; i < tf_velocitys.Length; i++){
           // print(" i " + i);
            if(i <= vel){
              //  print("on " + i);
                tf_velocitys[i].localScale = Vector3.one;
            }else{
               // print("off " + i);
                tf_velocitys[i].localScale = Vector3.zero;
            }


        }



	}



}
