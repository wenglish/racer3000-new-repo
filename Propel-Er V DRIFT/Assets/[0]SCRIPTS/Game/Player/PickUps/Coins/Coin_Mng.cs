﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;




public class Coin_Mng : MonoBehaviour {



    public static Coin_Mng ins;

    public static int coinsCollected;

    int localPlayerInt;
    PlayerUnit localPlayerUnit;


    void Awake(){


        if (ins == null)
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
        }else{
            Destroy(gameObject);
        }

 

    }



    public void setLocalPlayer(PlayerUnit localPlayerUnitDel, int localPlayerIntDel){
        ///sent from the CoinHud_Mng, of the local player. 
        /// we call it from there, as this, Coin Mng exists before level game play. 
        localPlayerInt = localPlayerIntDel;
        localPlayerUnit = localPlayerUnitDel;
    }






    public void coinPickedUp(int playerInt, int coins){

        //a player picked up a coin, before we pass on to the local player, must check if local player picked it up. 

        print("Coin Mng, at coin picked up ");
        if (playerInt == LoadingMng.localPlayerInt)
        {
            print("Coin Mng , at coin picked up by locla player,. ");
            ///then pass to this local player value to the local player and animate to hud. 
            addCoinToLocalPlayerCollection(coins);
        }
        else
        {
            print("COin mng at coin picked up by non local player. ");
            //the non local player has collected this, and caching the value is not required.
        }

    }
    void addCoinToLocalPlayerCollection(int coins){

        coinsCollected += coins;
        print("add tp coins total ->  " + coinsCollected.ToString());
        if(Game_Mng.ins != null){

            //thus we are in game scene. 
            //add coins to localPlayer coin HUD. 
            localPlayerUnit.coinHUD_Mng.addCoin();

        }


    }




}
