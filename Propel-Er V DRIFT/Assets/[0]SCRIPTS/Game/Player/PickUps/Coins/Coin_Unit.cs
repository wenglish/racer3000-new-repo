﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;



public class Coin_Unit : MonoBehaviour {



    Coin_Mng CM;
    public Collider cldr;
    int value = 1;

	// Use this for initialization
	void Start () {
        CM = Coin_Mng.ins;
        run();
	}

    void run(){
        transform.localEulerAngles = Vector3.zero;
        Vector3 rtn = new Vector3(0, 360, 0);
        transform.DORotate(rtn, 0.5f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);

    }

    private void OnTriggerEnter(Collider other)
    {
        int n = 0;
        if (other.CompareTag("Player1")){
            n = 1;
        }
        if (other.CompareTag("Player2")){
            n = 2;
        }

        if(n != 0){
            print("RUN :: Coin picked up by player " + name.ToString());
            //send player int and coin value. 
            CM.coinPickedUp(n, value);
            animatePickUp();
        }
        else{
            ///
            print("collided Coin does not = a player ");
        }

    }


    void animatePickUp(){
        cldr.enabled = false;
        float y = transform.position.y + 10;
        transform.DOMoveY(y, 0.25f, false).SetEase(Ease.InSine).OnComplete(end);
    }

	void end(){
        Destroy(gameObject);
	}



}
