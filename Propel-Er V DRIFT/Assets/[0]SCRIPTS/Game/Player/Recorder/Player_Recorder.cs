﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Recorder : MonoBehaviour
{


    public Transform tf_camP;
    public Transform tf_playerP;
    public Rigidbody rgbyPlayerP;


    public Transform tf_camR;
    public Transform tf_playerR;
    public Rigidbody rgbyPlayerR;


    [HideInInspector] public List<Vector3> pPos_list;
    [HideInInspector] public List<Quaternion> pRtn_list;

    [HideInInspector]public bool isRecord;
    [HideInInspector]public bool isPlayBack;

	private void Start()
	{
        tf_playerR.gameObject.SetActive(false);
        init_record();
        //tf_player = playerCntrlr.transform;
        //tf_cam = playerCntrlr.cameraFollow.transform;
        //rgbyPlayer = playerCntrlr.rgby;
	}


	private void Update()
    {
        if(Input.GetKeyUp(KeyCode.S)){
            init_record();
        }

        if(Input.GetKeyUp(KeyCode.D)){
            end_record();
        }

        if(Input.GetKeyUp(KeyCode.F)){
            init_playback();
        }
        if (Input.GetKeyUp(KeyCode.G))
        {
            end_playback();
        }
    }


	public void init_record() {
        if(isRecord){
        }else{
            pPos_list.Clear();
            pRtn_list.Clear();
            isPlayBack = false;
            isRecord = true;
        }
    }
    public void end_record(){
        if (!isRecord){

        }
        else{
            isRecord = false;
        }
    }


    int count_playback;
    public void init_playback()
    {
        if (isPlayBack || pPos_list.Count == 0){

        }
        else
        {
            tf_playerR.gameObject.SetActive(true);
            rgbyPlayerR.velocity = Vector3.zero;
            tf_playerR.position = pPos_list[0];
            tf_playerR.rotation = pRtn_list[0];
            count_playback = 0;
            isRecord = false;
            isPlayBack = true;
        }




    }
    public void end_playback()
    {
        rgbyPlayerR.velocity = Vector3.zero;
        isPlayBack = false;
    }


	void FixedUpdate()
	{
        if(isRecord){

            Vector3 pPos = tf_playerP.position;
            Quaternion pRtn = tf_playerP.rotation;


            pPos_list.Add(pPos);
            pRtn_list.Add(pRtn);

        }
        if(isPlayBack){
            Vector3 pPos = pPos_list[count_playback];
            Quaternion pRtn = pRtn_list[count_playback];
            float step = Time.deltaTime * 1;
            tf_playerR.position = Vector3.Lerp(tf_playerR.position, pPos, step);
            tf_playerR.rotation = Quaternion.Lerp(tf_playerR.rotation, pRtn, step);
            tf_camR.position = Vector3.Lerp(tf_playerR.position, pPos, step);
            tf_camR.rotation = Quaternion.Lerp(tf_playerR.rotation, pRtn, step);
            count_playback++;


            if(count_playback >= pPos_list.Count){

                end_playback();

            }
        }

	}











}
