﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.SceneManagement;



public class LoadingMng : MonoBehaviour {

    public static LoadingMng ins;


    public Transform tf_curtains;
    [HideInInspector]public Vector3 pos_offScreen = new Vector3 (0, 0, -25);

    public TextMeshPro tmp_loading;

    public static float transitionTime = 0.5f;

    public static bool isRecordPlay;

    public static bool isTutorial;



    //used as ref thru game play in multi. 
    public static int localPlayerInt;
    public static int gameModeInt;//1 = offline solo, 2 = online multi, 3 = practice. 
    public static int subGameModeInt;//there are an array of game modes, set in Home Scene, under the SoloGameOptions_Mng. 


	private void Awake()
	{
        if(LoadingMng.ins == null){
            LoadingMng.ins = this;
            DontDestroyOnLoad(gameObject);
            return;
        }
        else{
            Destroy(gameObject);
        }
	}


	// Use this for initialization
	void Start () {
       set_curtains();
	}
    void set_curtains(){
        tf_curtains.position = Vector2.zero;
    }


    public void runInCurtain(float delay){
        runTrans(tf_curtains, Vector3.zero, Vector3.one, delay, Ease.OutBounce);
    }
    public void runOutCurtain(float delay)
    {
        runTrans(tf_curtains, pos_offScreen, Vector3.one, delay, Ease.InBounce);
    }
    void runTrans(Transform tf, Vector3 pos, Vector3 scl, float delay, Ease ease)
    {
        tf.DOMove(pos, transitionTime, false).SetEase(ease).SetDelay(delay);
        tf.DOScale(scl, transitionTime).SetEase(ease).SetDelay(delay);
    }



    int levelToLoad;
    public void init_sceneChange(int level, float delay){

        levelToLoad = level;
        Invoke("changeScene", delay);

    }

    void changeScene(){
        SceneManager.LoadScene(levelToLoad);
    }





}
