﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;



public class GridMaster : MonoBehaviour {


    public MeshRenderer renderer;




    public Renderer rndr_topDummy;
    public Renderer rndr_btmDummy;
    public Color[] c_topColors;
    Color c_topTrgt;
    Color c_btmTrgt;
    [Space]
    public Color c_topCrnt;
    public Color c_btmCrnt;



	private void Start(){


        renderer.material.DOOffset(Vector2.up, 1).SetLoops(-1, LoopType.Incremental);
        //initColorTweens();
	}


    void initColorTweens(){


        //materialTop = topRenderer.material;
        //topRenderer.material.DOColor(colorTop, 1).SetLoops(-1, LoopType.Yoyo);
        //materialBottom = bottomRenderer.material;
        //bottomRenderer.material.DOColor(colorBottom, 1).SetLoops(-1, LoopType.Yoyo);


        //handle Color.
        c_topTrgt = c_topColors[1];
        c_btmTrgt = c_topColors[0];


        c_topCrnt = c_topTrgt;
        c_btmCrnt = c_btmTrgt;

        renderer.material.SetColor("_TopColor", c_topCrnt);
        renderer.material.SetColor("_BottomColor", c_btmCrnt);


        rndr_topDummy.material.color = c_btmCrnt;
        rndr_btmDummy.material.color = c_topCrnt;


        rndr_topDummy.material.DOColor(c_topTrgt, 10).SetLoops(-1, LoopType.Yoyo);
        rndr_btmDummy.material.DOColor(c_btmCrnt, 10).SetLoops(-1, LoopType.Yoyo);


    }


	// Update is called once per frame
	void Update () {
        if(renderer.material != null){


            ///handle UVs 
            Vector2 crntOffset = renderer.material.GetTextureOffset("_GridTexture");
            //print("crntOffset = " + crntOffset);
            Vector2 trgtOffset = new Vector2(crntOffset.x, crntOffset.y + (2.5f * -Time.deltaTime));
            //print("trgtOffset = " + trgtOffset);
            if(trgtOffset.y <= 0){
                trgtOffset = Vector2.up;
            }
            renderer.material.SetTextureOffset("_GridTexture", trgtOffset);



            c_topCrnt = rndr_topDummy.material.color;
            c_btmCrnt = rndr_btmDummy.material.color;

            //renderer.material.SetColor("_TopColor", c_topCrnt);
            //renderer.material.SetColor("_BottomColor", c_btmCrnt);

        }
	}
}
