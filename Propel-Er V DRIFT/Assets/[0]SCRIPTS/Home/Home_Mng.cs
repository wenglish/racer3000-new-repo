﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.SceneManagement;




namespace Com.YahManGames.P3K
{
    public class Home_Mng : MonoBehaviour
    {

        public static Home_Mng ins;

        public Transform tf_mainLogo;
        public Transform tf_mainBtns;
        public Transform tf_alert;
        public TextMeshPro tmp_alert;




        [HideInInspector] public Vector3 posStart_mainLogo = new Vector3(2.3f, -0.86f, 1.24f);
        [HideInInspector] public Vector3 sclStart_mainLogo = new Vector3(0.8844743f, 0.8844743f, 0.8844743f);
        [HideInInspector] public Vector3 posEnd_mainLogo = new Vector3(2.3f, 5.74f, -2.06f);
        [HideInInspector] public Vector3 sclEnd_mainLogo = new Vector3(0.8844743f, 0.8844743f, 0.8844743f);


        [HideInInspector] public Vector3 posHide_up = new Vector3(0, 45, 0);
        [HideInInspector] public Vector3 posHide_down = new Vector3(0, -15, 0);
        float transitionTime;





        private void Awake()
        {
            ins = this;
        }




        private void Start()
        {

            tf_mainLogo.position = posStart_mainLogo;
            tf_mainLogo.localScale = sclStart_mainLogo;
            tf_mainBtns.position = posHide_down;
            tf_alert.position = posHide_up;

            transitionTime = LoadingMng.transitionTime;

            runTrans(tf_mainLogo, posEnd_mainLogo, sclEnd_mainLogo, 2.5f, Ease.InOutCirc);
            runTrans(tf_mainBtns, Vector3.zero, Vector3.one, 3, Ease.OutBounce);

            LoadingMng.ins.runOutCurtain(1f);

        }




        //with no text
        public void runTrans(Transform tf, Vector3 pos, Vector3 scl, float delay, Ease ease)
        {
            tf.DOMove(pos, transitionTime, false).SetEase(ease).SetDelay(delay);
            tf.DOScale(scl, transitionTime).SetEase(ease).SetDelay(delay);
        }
        //
        void runText(TextMeshPro tmp, string s)
        {
            tmp.text = s;
        }










        [HideInInspector] public List<string> s_alerts;
        int alertCount;
        int alertLimit;
        public void init_animateAlert(List<string> s, float frq)
        {
            print("init AnimateAlert() ");
            for (int i = 0; i < s.Count; i++)
            {

                print("s = " + s[i]);

            }
            CancelInvoke("animateAlert");
            set_sAlert(s);
            alertCount = 0;
            InvokeRepeating("animateAlert", frq, frq);
        }
        void set_sAlert(List<string> s)
        {
            print("set_sAlert");
            s_alerts.Clear();
            s_alerts = s;
            alertLimit = s_alerts.Count;
            print("count " + alertCount + "/" + alertLimit);
        }
        void animateAlert()
        {
            tmp_alert.text = s_alerts[alertCount];
            alertCount++;
            if (alertCount >= alertLimit)
            {
                alertCount = 0;
            }
        }
        public void cancelAlertAnim(){
            CancelInvoke("animateAlert");
        }










        public void bp_help(){

            LoadingMng.isTutorial = true;
            LoadingMng.gameModeInt = 1;
            LoadingMng.localPlayerInt = 1;
            init_LoadLevel(1, 0.25f);


        }








        public void bp_Play()
        {

            runTrans(tf_mainBtns, posHide_down, Vector3.one, 0f, Ease.InBounce);
            GameOptions_Mng.ins.bringIn();





        }








        public void onlineOpponentFound()
        {
            //return from Launcher.ins, local and remote players found, loa multi player. 
            LoadingMng.gameModeInt = 2;

            if (PhotonNetwork.isMasterClient)
            {
                //LoadingMng.localPlayerInt = 1;
            }
            else
            {
                //LoadingMng.localPlayerInt = 2;
            }

            init_LoadLevel(1, 3);

        }
        public void noOnlineOpponentFound()
        {
            //return from Launcher.ins, no online opponent found in time limit. 

            //at this point the GameMngOptions 



            LoadingMng.gameModeInt = 1;
            LoadingMng.localPlayerInt = 1;
            init_LoadLevel(1, 3);


        }




















        int levelToLoad;
        void init_LoadLevel(int level, float delay)
        {


            print("init load level : game mode = " + LoadingMng.gameModeInt);
            levelToLoad = level;



            Invoke("loadLevel", delay);


            runTrans(tf_alert, posHide_up, Vector3.one, delay / 4, Ease.OutCirc);
            runTrans(tf_mainLogo, posStart_mainLogo, sclStart_mainLogo, delay / 4, Ease.OutCirc);
            LoadingMng.ins.runInCurtain(delay / 2);



        }

        void loadLevel()
        {




            if (LoadingMng.gameModeInt == 1)
            {
                print("load level locally");
                SceneManager.LoadScene(levelToLoad);
            }
            else if (LoadingMng.gameModeInt == 2)
            {
                if (PhotonNetwork.isMasterClient)
                {
                    print("load level over network.");
                    PhotonNetwork.LoadLevel(levelToLoad);
                }
            }

        }








    }



}
