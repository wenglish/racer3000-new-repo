﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridAnimMng : MonoBehaviour {

    public Transform[] tf_units;
    public float stepFctr;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {


        for (int i = 0; i < tf_units.Length; i++)
        {


            float x = tf_units[i].localPosition.x;
            if (x <= -86)
            {
                tf_units[i].localPosition = new Vector3(154, tf_units[i].localPosition.y, tf_units[i].localPosition.z);
            }


            tf_units[i].Translate(Vector3.left * Time.deltaTime * stepFctr);



        }


	}
}
