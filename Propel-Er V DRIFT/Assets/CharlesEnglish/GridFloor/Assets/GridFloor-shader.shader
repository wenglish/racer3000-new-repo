// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32836,y:32737,varname:node_3138,prsc:2|emission-7671-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:31847,y:33146,ptovrint:False,ptlb:BottomColor,ptin:_BottomColor,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Tex2dAsset,id:9630,x:31567,y:33236,ptovrint:False,ptlb:GridTexture,ptin:_GridTexture,varname:node_9630,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:3843,x:31836,y:32910,varname:node_3843,prsc:2,ntxv:0,isnm:False|UVIN-1848-OUT,TEX-9630-TEX;n:type:ShaderForge.SFN_Parallax,id:5029,x:31400,y:33601,varname:node_5029,prsc:2|UVIN-1700-OUT,HEI-9948-OUT;n:type:ShaderForge.SFN_TexCoord,id:9388,x:30594,y:33456,varname:node_9388,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Tex2d,id:2958,x:31847,y:33298,varname:node_2958,prsc:2,ntxv:0,isnm:False|UVIN-4339-OUT,TEX-9630-TEX;n:type:ShaderForge.SFN_Color,id:2905,x:31836,y:32740,ptovrint:False,ptlb:TopColor,ptin:_TopColor,varname:node_2905,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:6715,x:31029,y:33698,ptovrint:False,ptlb:Parralax Depth,ptin:_ParralaxDepth,varname:node_6715,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Lerp,id:6270,x:32173,y:33272,varname:node_6270,prsc:2|A-6141-OUT,B-7241-RGB,T-2958-G;n:type:ShaderForge.SFN_Vector4,id:6141,x:31887,y:33479,varname:node_6141,prsc:2,v1:0,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_Lerp,id:3424,x:32180,y:32930,varname:node_3424,prsc:2|A-6141-OUT,B-2905-RGB,T-3843-R;n:type:ShaderForge.SFN_Lerp,id:7671,x:32412,y:33007,varname:node_7671,prsc:2|A-6270-OUT,B-3424-OUT,T-3843-R;n:type:ShaderForge.SFN_Add,id:7904,x:32448,y:33297,varname:node_7904,prsc:2|A-3843-R,B-2958-G;n:type:ShaderForge.SFN_Clamp01,id:5649,x:32653,y:33279,varname:node_5649,prsc:2|IN-7904-OUT;n:type:ShaderForge.SFN_Multiply,id:4339,x:31610,y:33521,varname:node_4339,prsc:2|A-4024-OUT,B-5029-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:4024,x:31153,y:33288,ptovrint:False,ptlb:Grid Scale,ptin:_GridScale,varname:node_4024,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:1848,x:31575,y:32962,varname:node_1848,prsc:2|A-1700-OUT,B-4024-OUT;n:type:ShaderForge.SFN_OneMinus,id:9948,x:31187,y:33698,varname:node_9948,prsc:2|IN-6715-OUT;n:type:ShaderForge.SFN_Time,id:2552,x:30231,y:33754,varname:node_2552,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1232,x:30459,y:33717,varname:node_1232,prsc:2|A-6789-OUT,B-2552-T;n:type:ShaderForge.SFN_ValueProperty,id:6789,x:30231,y:33648,ptovrint:False,ptlb:Scroll Speed,ptin:_ScrollSpeed,varname:node_6789,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:7612,x:30842,y:33495,varname:node_7612,prsc:2|A-9388-V,B-1232-OUT;n:type:ShaderForge.SFN_Append,id:1700,x:31045,y:33426,varname:node_1700,prsc:2|A-9388-U,B-7612-OUT;proporder:2905-7241-6715-9630-4024-6789;pass:END;sub:END;*/

Shader "CharlesEnglish/GridFloor" {
    Properties {
        _TopColor ("TopColor", Color) = (0.5,0.5,0.5,1)
        _BottomColor ("BottomColor", Color) = (0.07843138,0.3921569,0.7843137,1)
        _ParralaxDepth ("Parralax Depth", Float ) = 0
        _GridTexture ("GridTexture", 2D) = "white" {}
        _GridScale ("Grid Scale", Float ) = 1
        _ScrollSpeed ("Scroll Speed", Float ) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _BottomColor;
            uniform sampler2D _GridTexture; uniform float4 _GridTexture_ST;
            uniform float4 _TopColor;
            uniform float _ParralaxDepth;
            uniform float _GridScale;
            uniform float _ScrollSpeed;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_6141 = float4(0,0,0,0);
                float4 node_2552 = _Time;
                float node_1232 = (_ScrollSpeed*node_2552.g);
                float2 node_1700 = float2(i.uv0.r,(i.uv0.g+node_1232));
                float2 node_4339 = (_GridScale*(0.05*((1.0 - _ParralaxDepth) - 0.5)*mul(tangentTransform, viewDirection).xy + node_1700).rg);
                float4 node_2958 = tex2D(_GridTexture,TRANSFORM_TEX(node_4339, _GridTexture));
                float2 node_1848 = (node_1700*_GridScale);
                float4 node_3843 = tex2D(_GridTexture,TRANSFORM_TEX(node_1848, _GridTexture));
                float3 emissive = lerp(lerp(node_6141,float4(_BottomColor.rgb,0.0),node_2958.g),lerp(node_6141,float4(_TopColor.rgb,0.0),node_3843.r),node_3843.r).rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
